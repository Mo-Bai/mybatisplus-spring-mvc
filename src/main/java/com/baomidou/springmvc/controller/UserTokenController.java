package com.baomidou.springmvc.controller;


import com.alibaba.fastjson.JSONObject;
import com.baomidou.mybatisplus.mapper.EntityWrapper;
import com.baomidou.springmvc.model.UserToken;
import com.baomidou.springmvc.service.UserTokenService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestMapping;

import org.springframework.stereotype.Controller;
import com.baomidou.springmvc.controller.BaseController;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;

import java.util.List;

/**
 * <p>
 *  前端控制器
 * </p>
 *
 * @author wu
 * @since 2018-08-21
 */
@Controller
@RequestMapping("/userToken")
public class UserTokenController extends BaseController {
    @Autowired
    private UserTokenService userTokenService;

    @ResponseBody
    @RequestMapping("/queryToken")
    public Object queryToken(@RequestParam(value = "id", required = true) Long id) {
        //itUserService.selectList(Wrapper<TUser >.)
        EntityWrapper<UserToken> ew = new EntityWrapper<UserToken>();
        ew.setEntity(new UserToken());
        //ew.where("id={0} or user_id={1}",2,1);
        ew.eq("user_id",id);
        List<UserToken> list = userTokenService.selectList(ew);
        System.out.println("Sql = " + ew.getSqlSegment());
        System.out.println(JSONObject.toJSONString(list));
        return userTokenService.selectById(id);
    }
}

