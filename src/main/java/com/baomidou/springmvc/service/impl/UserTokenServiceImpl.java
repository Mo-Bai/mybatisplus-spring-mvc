package com.baomidou.springmvc.service.impl;

import com.baomidou.springmvc.model.UserToken;
import com.baomidou.springmvc.mapper.UserTokenMapper;
import com.baomidou.springmvc.service.UserTokenService;
import com.baomidou.mybatisplus.service.impl.ServiceImpl;
import org.springframework.stereotype.Service;

/**
 * <p>
 *  服务实现类
 * </p>
 *
 * @author wu
 * @since 2018-08-21
 */
@Service
public class UserTokenServiceImpl extends ServiceImpl<UserTokenMapper, UserToken> implements UserTokenService {

}
