package com.baomidou.springmvc.service;

import com.baomidou.springmvc.model.UserToken;
import com.baomidou.mybatisplus.service.IService;

/**
 * <p>
 *  服务类
 * </p>
 *
 * @author wu
 * @since 2018-08-21
 */
public interface UserTokenService extends IService<UserToken> {

}
