package com.baomidou.springmvc.service;

import com.baomidou.mybatisplus.service.IService;
import com.baomidou.springmvc.model.User;

/**
 *
 * User 表数据服务层接口
 *
 */
public interface IUserService extends IService<User> {


}