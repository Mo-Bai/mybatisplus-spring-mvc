package com.baomidou.springmvc.mapper;

import com.baomidou.springmvc.common.SuperMapper;
import com.baomidou.springmvc.model.User;

/**
 *
 * User 表数据库控制层接口
 *
 */
public interface UserMapper extends SuperMapper<User> {


}
