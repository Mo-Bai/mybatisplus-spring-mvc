package com.baomidou.springmvc.mapper;

import com.baomidou.springmvc.model.UserToken;
import com.baomidou.springmvc.common.SuperMapper;

/**
 * <p>
 *  Mapper 接口
 * </p>
 *
 * @author wu
 * @since 2018-08-21
 */
public interface UserTokenMapper extends SuperMapper<UserToken> {

}
